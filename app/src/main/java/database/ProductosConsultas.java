package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ProductosConsultas {
    private Context context;
    private ProductoDbHelper productoDbHelper;
    private SQLiteDatabase db;

    private String [] columnToRead = new String[]{
            DefinirTabla.Producto._ID,
            DefinirTabla.Producto.NOMBRE,
            DefinirTabla.Producto.MARCA,
            DefinirTabla.Producto.PRECIO,
            DefinirTabla.Producto.PERECEDERO,
    };
    public ProductosConsultas(Context context){
        this.context=context;
        productoDbHelper = new ProductoDbHelper(this.context);
    }

    public void openDataBase(){
        db=productoDbHelper.getWritableDatabase();

    }
    public long insertarContacto(Producto p){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.NOMBRE,p.getNombre());
        values.put(DefinirTabla.Producto.MARCA,p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO,p.getPrecio());
        values.put(DefinirTabla.Producto.PERECEDERO,p.getPerecedero());
        return db.insert(DefinirTabla.Producto.TABLE_NAME,null,values);
    }
    public long updateProducto(Producto p,int codigo){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.NOMBRE,p.getNombre());
        values.put(DefinirTabla.Producto.MARCA,p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO,p.getPrecio());
        values.put(DefinirTabla.Producto.PERECEDERO,p.getPerecedero());
        String criterio = DefinirTabla.Producto._ID+" = " + codigo;
        return db.update(DefinirTabla.Producto.TABLE_NAME,values,criterio,null);
    }

    public int deleteProducto(int id){
        String criterio = DefinirTabla.Producto._ID+" = " + id;
        return db.delete(DefinirTabla.Producto.TABLE_NAME,criterio,null);
    }
    public Producto readProducto(Cursor cursor){
        Producto p = new Producto();
        p.setCodigo(cursor.getInt(0));
        p.setNombre(cursor.getString(1));
        p.setMarca(cursor.getString(2));
        p.setPrecio(cursor.getFloat(3));
        p.setPerecedero(cursor.getInt(4));
        return p;
    }
    public Producto getProducto(int id){
        SQLiteDatabase db =  productoDbHelper.getReadableDatabase();
        Cursor p = db.query(DefinirTabla.Producto.TABLE_NAME,columnToRead, DefinirTabla.Producto._ID +
                " = ?",new String[]{String.valueOf(id)},null,null,null);
        p.moveToFirst();
        Producto producto = readProducto(p);
        p.close();
        return producto;
    }

    public ArrayList<Producto> allContactos(){
        ArrayList<Producto> productos = new ArrayList<Producto>();
        Cursor cursor = db.query(DefinirTabla.Producto.TABLE_NAME,
                null,null,null,null,null,null);
         cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Producto p = readProducto(cursor);
            productos.add(p);
            cursor.moveToNext();
        }
        cursor.close();
        return productos;

    }
    public void cerrar(){
        productoDbHelper.close();
    }
}
