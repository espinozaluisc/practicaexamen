package database;

import android.provider.BaseColumns;

public class DefinirTabla {

    public DefinirTabla() {
    }

    public static abstract class Producto implements BaseColumns{
        public static final String TABLE_NAME="productos";
        public static final String NOMBRE="nombre";
        public static final String MARCA="marca";
        public static final String PRECIO="precio";
        public static final String PERECEDERO="perecedero";
    }
}
