package database;

import java.io.Serializable;

public class Producto implements Serializable {

    private int codigo;
    private String nombre;
    private String marca;
    private float precio;
    private int perecedero;

    public Producto(int codigo, String nombre, String marca, float precio, int perecedero) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
        this.perecedero = perecedero;
    }

    public Producto() {
    }

    public int getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getMarca() {
        return marca;
    }

    public float getPrecio() {
        return precio;
    }

    public int getPerecedero() {
        return perecedero;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public void setPerecedero(int perecedero) {
        this.perecedero = perecedero;
    }
}
